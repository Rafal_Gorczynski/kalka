
import Foundation

class Task {
    let textField: String
    let date: String
    
    init (textField: String, date: String){
        self.textField = textField
        self.date = date
    }
}
