//
//  CustomDateFormatter.swift
//  Kalka ToDo
//
//  Created by Rafal Gorczynski on 04.12.2016.
//  Copyright © 2016 Rafal Gorczynski. All rights reserved.
//

import Foundation

class CustomDateFormatter: DateFormatter {
    
    func currentDateString() -> String {
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: currentDate)
        return date
    }
}
