
import UIKit

class TableViewController: UITableViewController {
    @IBAction func addTaskButton(_ sender: Any) {
        self.performSegue(withIdentifier: "goToTasksSegue", sender: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataMenager.shared.tasks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath)

        cell.textLabel?.text = DataMenager.shared.tasks[indexPath.row].textField
        cell.detailTextLabel?.text = DataMenager.shared.tasks[indexPath.row].date

        return cell
    }
}
