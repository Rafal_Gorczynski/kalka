
import UIKit

class AddTaskViewController: UIViewController {

    var taskDate: String?
    @IBOutlet weak var taskTextField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var dateButton: UIButton!

    @IBAction func dateButtonAction(_ sender: Any) {
        if taskDate == nil {
            taskDate = CustomDateFormatter().currentDateString()
        }

        dateButton.setTitle(taskDate, for: .normal)

        datePicker.alpha = datePicker.alpha != 0.0 ? 0 : 1
    }

    @IBAction func datePickerAction(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        taskDate = dateFormatter.string(from: datePicker.date)
    }

    @IBAction func addTaskBarButton(_ sender: Any) {
        if (taskTextField.text?.characters.count)! > 0 {
            //zrobic z tego funkcje
            if taskDate == nil {
                taskDate = CustomDateFormatter().currentDateString()
            }
            DataMenager.shared.tasks.append(Task(textField: taskTextField.text!, date: taskDate!))
        self.dismiss(animated: true, completion: nil)            
        }
    }
}
